#Restaurant Hosting Website
#React Application using express and MongoDB

This React Application has been built as part of Coursera's, Server Side Development with NodeJS,Express and MongoDB,course which is part of its
Full Stack Web and Multiplatform App Development specialization.

##Requirements-
1.NodeJS
2.React
3.Express(for server-side development)
4.MongoDB